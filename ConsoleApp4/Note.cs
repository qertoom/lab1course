﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Note
    {
        public int id;
        public string LastName;
        public string FirstName;
        public string ThirdName;
        public string PhoneNumber;
        public string Country;
        public string BirthDate;
        public string Organisaton;
        public string Position;
        public string Other;

        public Note() { }

        public Note(int id, string LastName, string FirstName, string ThirdName, string PhoneNumber, string Country, string BirthDate, string Organisaton, string Position, string Other)
        {
            this.id = id;
            this.LastName = LastName;
            this.FirstName = FirstName;
            this.ThirdName = ThirdName;
            this.PhoneNumber = PhoneNumber;
            this.Country = Country;
            this.BirthDate = BirthDate;
            this.Organisaton = Organisaton;
            this.Position = Position;
            this.Other = Other;

        }
        public override string ToString()
        {
            return Convert.ToString($"№:{id} Фамилия:{LastName} Имя:{FirstName} Отчество:{ThirdName} Номер телефона:{PhoneNumber} Страна:{Country} Дата рождения:{BirthDate} Организация:{Organisaton} Должность:{Position} Прочие заметки:{Other}");
        }
    }
}
