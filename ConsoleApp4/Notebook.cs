﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


// ..
namespace ConsoleApp4
{
    class Notebook
    {
        public static List<Note> notebook = new List<Note>();
        public static int id = 0;
        public static void Main(string[] args)
        {
            Greeter();
        }




        public static void CreateNewNote()
        {
            Console.Clear();
            Console.WriteLine("Введи фамилию");
            var LastName = Console.ReadLine();
            while (LastName == "")
            {
                Console.WriteLine("Это поле является обязательным! Повторите ввод");
                Console.WriteLine("Введи фамилию");
                LastName = Console.ReadLine();
            }
            Console.WriteLine("Введи имя");
            var FirstName = Console.ReadLine();
            while (FirstName == "")
            {
                Console.WriteLine("Это поле является обязательным! Повторите ввод");
                Console.WriteLine("Введи имя");
                FirstName = Console.ReadLine();
            }
            Console.WriteLine("Введи отчество");
            var ThirdName = Console.ReadLine();
            Console.WriteLine("Введи номер телефона");
            var PhoneNumber = Console.ReadLine();
            while (PhoneNumber == "")
            {
                Console.WriteLine("Это поле является обязательным! Повторите ввод");
                Console.WriteLine("Введи номер телефона");
                PhoneNumber = Console.ReadLine();
            }
            Console.WriteLine("Введи страну");
            var Country = Console.ReadLine();
            while (Country == "")
            {
                Console.WriteLine("Это поле является обязательным! Повторите ввод");
                Console.WriteLine("Введи страну");
                Country = Console.ReadLine();
            }
            Console.WriteLine("Введи дату рождения");
            var BirthDate = Console.ReadLine();
            Console.WriteLine("Введи организацию");
            var Organisaton = Console.ReadLine();
            Console.WriteLine("Введи должность");
            var Position = Console.ReadLine();
            Console.WriteLine("Введи прочие заметки");
            var Other = Console.ReadLine();
            Note newNote = new Note(id, LastName, FirstName, ThirdName, PhoneNumber, Country, BirthDate, Organisaton, Position, Other);
            notebook.Add(newNote);
            Console.WriteLine(newNote.ToString());
        }
        public static void EditNote()
        {
            Console.Clear();
            int UserChoice = 0;
            foreach (Note c in notebook)
            {
                Console.WriteLine(c.ToString());
            }
            Console.WriteLine("Выберите номер записи, которую хотите изменить");
            int IdChoice = ReadNumber();
            foreach (Note c in notebook)
            {

                if (id == IdChoice)
                {
                    Console.WriteLine("Выберите, что вы хотите изменить: \n" +
                "1 - Фамилию \n" +
                "2 - Имя \n" +
                "3 - Отчество \n" +
                "4 - Номер телефона \n" +
                "5 - Страну \n" +
                "6 - Дату рождения \n" +
                "7 - Организацию \n" +
                "8 - Позицию \n" +
                "9 - Прочие заметки \n" +
                "0 - Вернуться в главное меню \n");
                    UserChoice = ReadNumber();
                    Console.Clear();
                    Console.WriteLine("Введите значение, которое хотите поместить");
                    string UserString = Console.ReadLine();
                    switch (UserChoice)
                    {
                        case 1:
                            c.LastName = UserString;
                            break;
                        case 2:
                            c.FirstName = UserString;
                            break;
                        case 3:
                            c.ThirdName = UserString;
                            break;
                        case 4:
                            c.PhoneNumber = UserString;
                            break;
                        case 5:
                            c.Country = UserString;
                            break;
                        case 6:
                            c.BirthDate = UserString;
                            break;
                        case 7:
                            c.Organisaton = UserString;
                            break;
                        case 8:
                            c.Position = UserString;
                            break;
                        case 9:
                            c.Other = UserString;
                            break;
                        case 0:
                            Greeter();
                            break;
                        default:
                            Greeter();
                            break;
                    }
                    Console.WriteLine(c.ToString());
                }
            }



        }

        public static void DeleteNote()
        {
            Console.Clear();
            foreach (Note c in notebook)
            {
                Console.WriteLine(c.ToString());
            }
            Console.WriteLine("Выберите номер записи, которую хотите удалить");
            int IdChoice = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < notebook.Count; i++)
            {
                if (notebook[i].id == IdChoice)
                {
                    notebook.Remove(notebook[i]);
                    break;

                }
            }
        }

        public static void ReadNote()
        {
            Console.Clear();
            foreach (Note c in notebook)
            {
                Console.WriteLine(c.ToString());
            }
        }

        public static void ShowAllNotes()
        {
            Console.Clear();
            foreach (Note c in notebook)
            {
                Console.WriteLine((Convert.ToString(id)) + " " + c.LastName + " " + c.FirstName + " " + c.PhoneNumber);
            }

        }
        public static int ReadNumber()
        {

            int numReader = 0;
            while (true)
            {
                try
                {
                    numReader = Convert.ToInt32(Console.ReadLine());
                    return numReader;
                }
                catch (Exception x)
                {
                    Console.WriteLine("Некоректный ввод! \n Повторите ввод ввод");
                }
            }


        }

        public static void Greeter()
        {
            while (true)
            {
                Console.WriteLine("Привет, я твоя записная книжка! Выбери, что ты хочешь сделать: \n");
                Console.WriteLine("1 - Создать новую запись \n" +
                    "2 - Редактировать созданную запись \n" +
                    "3 - Удалить запись \n" +
                    "4 - Посмотреть созданные записи \n" +
                    "5 - Посмотреть все созданные записи с краткой информацией (фамилия, имя, номер телефона) \n");

                int UserInput = ReadNumber();

                switch (UserInput)
                {
                    case 1:
                        id++;
                        CreateNewNote();
                        break;
                    case 2:
                        EditNote();
                        break;
                    case 3:
                        DeleteNote();
                        break;
                    case 4:
                        ReadNote();
                        break;
                    case 5:

                        break;
                    default:
                        Greeter();
                        break;
                }


                Console.ReadKey();
            }

        }


    }
}
